<!doctype html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Bootstrap 4 Template with Sidebar Menu</title>

    <!-- CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500&display=swap">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/media-queries.css">
    <link rel="stylesheet" href="../assets/css/todo.css">


    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>
    <!-- Wrapper -->
    <div class="wrapper">

        <!-- Sidebar -->
        <nav class="sidebar">

            <!-- close sidebar menu -->
            <div class="dismiss">
                <i class="fas fa-arrow-left"></i>
            </div>

            <div class="logo">
                <h3><a href="trash.php">Welcome to Keep.google.com</a></h3>
            </div>

            <ul class="list-unstyled menu-elements">
                <li class="active">
                    <a  href="../index.php"><i class="fas fa-home"></i> Home</a>
                </li>
                <li>
                    <a href="index.php"><i class="fas fa-cog"></i> To-Do</a>
                </li>
            </ul>

            <div class="to-top">
                <a class="btn btn-primary btn-customized-3" href="#" role="button">
                    <i class="fas fa-arrow-up"></i> Top
                </a>
            </div>

            <div class="dark-light-buttons">
                <a class="btn btn-primary btn-customized-4 btn-customized-dark" href="#" role="button">Dark</a>
                <a class="btn btn-primary btn-customized-4 btn-customized-light" href="#" role="button">Light</a>
            </div>

        </nav>
        <!-- End sidebar -->

        <!-- Dark overlay -->
        <div class="overlay"></div>

        <!-- Content -->
        <div class="content">

            <!-- open sidebar menu -->
            <a class="btn btn-primary btn-customized open-menu" href="#" role="button">
                <i class="fas fa-align-left"></i> <span>Menu</span>
            </a>

          

            <!-- Section 1 -->
            <div class="section-1-container section-container" id="section-1">
                <div class="container">
                    <div class="row">
                        <div class="col section-1 section-description wow fadeIn">
                            <h2>Restore Deleted Yet unfinished To Do lists!!</h2>
                            <div class="divider-1 wow fadeInUp"><span></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 section-1-box wow fadeInUp">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="section-1-box-icon">
                                        <i class="fas fa-book-open"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                <h3>Lists Your To Do's</h3>
                                    <p>Think no more of forgetting your tasks and assignments. This Site will help You manage that!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-1-box wow fadeInDown">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="section-1-box-icon">
                                        <i class="fas fa-pencil-alt"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                <h3>Edit Your ToDo's</h3>
                                    <p>Stop thinking about unplanned workloads, change it as soon as possible!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-1-box wow fadeInUp">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="section-1-box-icon">
                                        <i class="fas fa-magic"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                <h3>Delete Your ToDo's</h3>
                                    <p>Worry no more, You can delete unwanted or accomplished ToDo's! HORAAAAY!!!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- restored datas table -->
<div class="row justify-content-left">
<nav  class="navbar navbar-light bg-light">
	</nav>
	<div class="col-md-3"></div>
	<div class="col-md-6 well">
    <center><h3>My Deleted ToDO's</h3></center>
		<hr style="border-top:1px dotted #ccc;"/><br>
		<table class="table table-striped table-bordered table-hover">
			<thead class="alert-info">
				<tr>
					<th>Task</th>
					<th>Action</th> 
				</tr>
			</thead>
			<tbody style="background-color:white;">
				<?php
					require 'conn.php';
 
					$query = mysqli_query($conn, "SELECT * FROM `trash`") or die(mysqli_error());
					while($fetch = mysqli_fetch_array($query)){
				?>
				<tr>
					<td><?php echo $fetch['task']?></td>
					<td><a class="fas fa-restore" href="restore.php?id=<?php echo $fetch['id']?>"><span class="glyphicon glyphicon-reset"></span> Restore</a></td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
    </div>

                <!-- Footer -->
                <footer class="footer-container">

                    <div class="container">
                        <div class="row">

                            <div class="col">
                                &copy;emelisarafols ALL RIGHTS RESERVE:) 
                            </div>

                        </div>
                    </div>

                </footer>

            </div>
            <!-- End content -->

        </div>  
        <!-- End wrapper -->

        <!-- Javascript -->
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/jquery-migrate-3.0.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="../assets/js/jquery.backstretch.min.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.waypoints.min.js"></script>
        <script src="../assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="../assets/js/scripts.js"></script>

</body>

</html>






























